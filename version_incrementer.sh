#!/bin/bash

# Generate a new version based on old one and type of changes:
# - Major
# - Minor
# - Bugfix
# Author: Yann Kervran
# URL: https://framagit.org/YannKervran/Bareto

# HOW TO USE
# Copy this script in Git repository
# Make the script executable: chmod u+x version_incrementer.sh
# Run it, ie: ./version_incrementer.sh 1.0.0 bugfix
# The script returns the new version based upon:
# The choosen type :
# - major
# - minor
# - bugfix
# Semantic Versioning: https://semver.org/spec/v2.0.0.html

set -euo pipefail
VERSION="${1}"
MAJOR=0
MINOR=0
BUGFIX=0

# Break down the version number in components
regex="([0-9]+).([0-9]+).([0-9]+)"
if [[ ${VERSION} =~ ${regex} ]]; then
	MAJOR="${BASH_REMATCH[1]}"
	MINOR="${BASH_REMATCH[2]}"
	BUGFIX="${BASH_REMATCH[3]}"
fi

# Increment proper parameter, eventually restart lower ones
if [[ "${2}" == "bugfix" ]]; then
	BUGFIX=$(echo "${BUGFIX}" + 1 | bc)
elif [[ "${2}" == "minor" ]]; then
	MINOR=$(echo "${MINOR}" + 1 | bc)
	BUGFIX=0
elif [[ "${2}" == "major" ]]; then
	MAJOR=$(echo "${MAJOR}" + 1 | bc)
	MINOR=0
	BUGFIX=0
else
	echo "usage: ./${0}.sh [version-string] [major/minor/bugfix]"
	exit 0
fi

# Return new version
echo "${MAJOR}.${MINOR}.${BUGFIX}"
