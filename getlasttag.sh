#!/bin/bash

# Get latest tag from commits list
# Author: Yann Kervran
# URL: https://framagit.org/YannKervran/Bareto

# HOW TO USE
# Copy this script in Git repository
# Make the script executable: chmod u+x getlasttag.sh
# Run it: ./getlasttag.sh

GIT_VERSIONS=$(git tag -l --sort=-version:refname)
VERSIONS=($GIT_VERSIONS)
LATEST_VERSION=${VERSIONS[0]}

echo $LATEST_VERSION
