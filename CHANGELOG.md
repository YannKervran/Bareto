# Changelog
All merge request commits to this project will be documented in this file.
The project uses [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
The format is based on [Keep a Changelog](https://keepachangelog.com/).

## [6.0.0] - 2023-06-22
### Ci
- [4ed453a](https://framagit.org/YannKervran/Bareto/-/commit/4ed453a1dccf846cb949ad14d15df0ec1526433e) - Deleting tag of another Gitllab
- [cc584d4](https://framagit.org/YannKervran/Bareto/-/commit/cc584d4ea5d2906f5833ea771ab052122fcbb79e) - Adding debug
- [ea5ceea](https://framagit.org/YannKervran/Bareto/-/commit/ea5ceeac1313e24ff40d399e747adfdf0f7fab21) - Adding debug
- [e7b993f](https://framagit.org/YannKervran/Bareto/-/commit/e7b993fb661b2e23101e68c9a9680b7449d4ad96) - Adding a condition for first generation
- [3925617](https://framagit.org/YannKervran/Bareto/-/commit/3925617d8b42500b52e20c28d069be8e37877379) - Adding co ndition for first generation
- [276c5dc](https://framagit.org/YannKervran/Bareto/-/commit/276c5dc868e0ae71d839d480de1ebd6eaa7303ae) - Getting shell type
- [0af2198](https://framagit.org/YannKervran/Bareto/-/commit/0af219859e63cdf2b179ea3a74303f14dded1feb) - Adding cfg parameters and debug in CI
- [d0d5911](https://framagit.org/YannKervran/Bareto/-/commit/d0d59117369bc859b75f93212613bda4bb7e9da4) - Adding identity for the CI bot to commit
- [82b8257](https://framagit.org/YannKervran/Bareto/-/commit/82b8257f085f806dd6a48c583689883c7074bc5a) - Fix syntax error in variable calls
- [e5fff55](https://framagit.org/YannKervran/Bareto/-/commit/e5fff55f4b0ac14d7d0beb1aa8aabcdaf40cd529) - Adding logs
- [7586661](https://framagit.org/YannKervran/Bareto/-/commit/758666160fc17ed4c2965c56d7ef088c910f538f) - Adding some verbose
### Docs
- [a272f50](https://framagit.org/YannKervran/Bareto/-/commit/a272f502536a79fa0614b943a0007d0bfcaf4872) - Cleaning an detailing informations
### Feat
- [a513de0](https://framagit.org/YannKervran/Bareto/-/commit/a513de0e800c956aa3e39fafe6bbd979c2abd966) - Exclude ci commits of Changelog from changelog
- [a02367a](https://framagit.org/YannKervran/Bareto/-/commit/a02367a9b33df93bf1653a373216f70637dbffe8) - Cleaning and documenting
- [31c907b](https://framagit.org/YannKervran/Bareto/-/commit/31c907be9ef2b92252454ff5a0b43e4b03252ef1) - Adding a cfg file with repository parameters
### Fix
- [0670921](https://framagit.org/YannKervran/Bareto/-/commit/0670921a79756277a39acf0d5ef8c584462056af) - Adding day for title
- [d898cba](https://framagit.org/YannKervran/Bareto/-/commit/d898cba47af0b7b2f356ea2f381066e3db400204) - Proper title for initial untagged commits
- [6afce95](https://framagit.org/YannKervran/Bareto/-/commit/6afce952e3abcdc46f5170d0e7fc338273952361) - Reorganizing loops
- [6463879](https://framagit.org/YannKervran/Bareto/-/commit/6463879e203e1df5981d0d83edc7797ab20d3059) - Syntax error
- [93982ce](https://framagit.org/YannKervran/Bareto/-/commit/93982ceceffdf16eaace798a1b25c0251d6a7872) - Cleaning some parts
- [16ef15a](https://framagit.org/YannKervran/Bareto/-/commit/16ef15a962ba20e3049095b0a9c8308dcd458c9d) - Debug stuff
- [1c62d89](https://framagit.org/YannKervran/Bareto/-/commit/1c62d89b6c78ebb0c95e7102f4d9fc8353c32a37) - Adding initial untagged content
- [3090fab](https://framagit.org/YannKervran/Bareto/-/commit/3090fab4498c4d44795fe25042945e66befbcdf7) - Adding new commit to initial untag repository
- [26adc36](https://framagit.org/YannKervran/Bareto/-/commit/26adc3611deb024479c3ddc6adf8b84d200a0c03) - Cleaning debug lines
- [70186ef](https://framagit.org/YannKervran/Bareto/-/commit/70186ef6d7329af795d31a094b5cea49b2c08363) - Adding debug infos
- [91243bf](https://framagit.org/YannKervran/Bareto/-/commit/91243bf941fe8822a5d9d1974eb65417f31ec5b5) - Syntax error
- [2711906](https://framagit.org/YannKervran/Bareto/-/commit/2711906aa4fd42461e66d45f4a7de7fae5001084) - Adding former init content now tagged
- [88f82e8](https://framagit.org/YannKervran/Bareto/-/commit/88f82e864fd187125713a79b28b2d95e1cc83419) - Testing other loop
- [ee08255](https://framagit.org/YannKervran/Bareto/-/commit/ee08255489aebdc2556ddb7853e853d494290072) - Réécriture des parties intiales
- [82ec1b1](https://framagit.org/YannKervran/Bareto/-/commit/82ec1b12cc230527c33775a7131f9ef52bf5448b) - Adding debug info
- [008c703](https://framagit.org/YannKervran/Bareto/-/commit/008c703bb4cf2747f3c734d8d2a1c3061fb493e3) - Delete redundancy before first tag
- [c127496](https://framagit.org/YannKervran/Bareto/-/commit/c1274961ec91684578906637eec4c8433156e4b1) - Cleaning some variable
- [5c6e77b](https://framagit.org/YannKervran/Bareto/-/commit/5c6e77b6a98018ca714de2a616709735195b733f) - Change condition for initial typeup
- [1d49410](https://framagit.org/YannKervran/Bareto/-/commit/1d49410009ccf5ca1dcb6f1055e4f71329ff0da9) - Change chapter formulation
- [700d077](https://framagit.org/YannKervran/Bareto/-/commit/700d077f4d5ad264d1d6bd9d8e250c875b104676) - Fixing conditions for types of logs to create
- [59da2d5](https://framagit.org/YannKervran/Bareto/-/commit/59da2d5d53fd5768312701a371c8cd779599dd18) - Correcting an if statement
- [d3f1945](https://framagit.org/YannKervran/Bareto/-/commit/d3f19450e94c8061dbbe51476458727222d34c3e) - Modify default values
- [3d8f69b](https://framagit.org/YannKervran/Bareto/-/commit/3d8f69b89e9fd637ffe61c09b92033603f672436) - New algo to create base values for versions
- [08e6cb0](https://framagit.org/YannKervran/Bareto/-/commit/08e6cb06babab21706a1e23b2870de1be176bebc) - Adding default values to some scripts
- [464e51a](https://framagit.org/YannKervran/Bareto/-/commit/464e51ade21e4ffd77117d74424e7c01a5249595) - Adding debug infos
- [d0935e2](https://framagit.org/YannKervran/Bareto/-/commit/d0935e2f4fdae2b8c5d1a3af4df72d47edfe04b0) - Syntax error in a space
- [309920f](https://framagit.org/YannKervran/Bareto/-/commit/309920f41273053a4ecea2295b362bc858bfb010) - Adding a condition for initial creation
- [c6b222a](https://framagit.org/YannKervran/Bareto/-/commit/c6b222a955c5c14cbcd4481634031e731632dffc) - Debug info
- [f847933](https://framagit.org/YannKervran/Bareto/-/commit/f8479331e995d2c0ac0269a92867e6430b6a904c) - Delete quote marks
- [31367c8](https://framagit.org/YannKervran/Bareto/-/commit/31367c8f8e45e02accfdea8faa7f78ab80edcbfc) - Change input argument reading

## [5.0.1] - 2022-10-19
### Ci
- [6f4aea6](https://framagit.org/YannKervran/Bareto/-/commit/6f4aea65c42f4587a1043d771f82d3f5f4f3ed5d) - Legit title for CI automatic commit for CHANGELOG
### Docs
- [ed17764](https://framagit.org/YannKervran/Bareto/-/commit/ed17764c88ad1b6d6306adc531a0c87ca6cbde56) - Writing frist version of the documentation
- [72718f3](https://framagit.org/YannKervran/Bareto/-/commit/72718f3f7a5cae953fac502622cbe505a4083b5a) - Adding the section for parameters
- [ad6fc89](https://framagit.org/YannKervran/Bareto/-/commit/ad6fc893d4b8cb93cca99ccefd8509a29615de45) - Adding A-GPL licence
- [f8e79cf](https://framagit.org/YannKervran/Bareto/-/commit/f8e79cf789fdcdf522d8e89d43e5484431e72ebc) - Update of CHANGELOG
### Unclassified
- [9ade9c7](https://framagit.org/YannKervran/Bareto/-/commit/9ade9c7c05221859c1b8f20ca807d17b87884a21) - UpdatingCHANGELOG

## [5.0.0] - 2022-10-10
### Ci
- [f125e2b](https://framagit.org/YannKervran/Bareto/-/commit/f125e2b9b2ae36cac979e100ceff601fdffd71be) - Adding basics for CI usage of CHANGELOG generation
### Feat
- [ef1030e](https://framagit.org/YannKervran/Bareto/-/commit/ef1030e8c53aa839544018f5c58e73e6f1865bd9) - Adding --pretag parameter to have a proper display tagged
- [d26e86a](https://framagit.org/YannKervran/Bareto/-/commit/d26e86ac13edeb5809c6f270d90c8b7d87f7cb55) - Have titles with upperscale for first letter and no space before
### Style
- [0888287](https://framagit.org/YannKervran/Bareto/-/commit/0888287209444a7af9dfdd7ded5bf181290e19f1) - Deleting useless commentaries

## [4.1.1] - 2022-10-10
### Fix
- [8d0bfeb](https://framagit.org/YannKervran/Bareto/-/commit/8d0bfebeb2da971326e959b4e91784589ee3e563) - Proper display of chapters in CHANGELOG

## [4.1.0] - 2022-10-10
### Feat
- [2267f67](https://framagit.org/YannKervran/Bareto/-/commit/2267f672dd2b8a848e469857a9c965aee0561293) - Trying to have proper display of last tag

## [4.0.0] - 2022-10-10
### Feat
- [5219464](https://framagit.org/YannKervran/Bareto/-/commit/5219464c433fd674d3cd4b8d8e1fda03bdfef944) - Adding script for CI

## [3.0.0] - 2022-10-10
### Feat
- [27d27b2](https://framagit.org/YannKervran/Bareto/-/commit/27d27b21fa5c57c69037065745e3fd94efef33ab) - Adding script to get the major/minor/bugfix type of next version

## [2.0.0] - 2022-10-10
### Feat
- [cf8c51c](https://framagit.org/YannKervran/Bareto/-/commit/cf8c51cc9b6114aacc9ccf90d3d84a7bb33896e8) - Adding mention of the next version to come in changelog, determined automatically
- [076dd02](https://framagit.org/YannKervran/Bareto/-/commit/076dd0251bde1742ff6878fd2e5aa761d6032832) - Adding the version incrementer script
### Fix
- [021a187](https://framagit.org/YannKervran/Bareto/-/commit/021a18763085cda4b237ab7f97c17d1ceaa49b87) - Display proper category, even for breaking changes

## [1.3.1] - 2022-10-10
### Refactor
- [b5faf4a](https://framagit.org/YannKervran/Bareto/-/commit/b5faf4a8860ad1bb60423811262d707c11f3874d) - New way to get proper next version type

## [1.3.0] - 2022-10-10
### Feat
- [f11d91e](https://framagit.org/YannKervran/Bareto/-/commit/f11d91ebd4c4d3857a3fbc57a7c2b3096b89312a) - Working onf scripts for CI
### Fix
- [3f6b936](https://framagit.org/YannKervran/Bareto/-/commit/3f6b936d1e50f0aabe852abfc8ea912fc6909f34) - New script for determining type of version change from commits
### Unclassified
- [9745ac3](https://framagit.org/YannKervran/Bareto/-/commit/9745ac3b8bf7de9944c1ddfda5e7f56f213b3b5f) - Nouveauscript ajouté

## [1.2.0] - 2022-09-27
### Fix
- [73ebe00](https://framagit.org/YannKervran/Bareto/-/commit/73ebe006b4f94e3d75b48ab32ab7f48a50219302) - Deleting obsolete file
### Unclassified
- [dfc13db](https://framagit.org/YannKervran/Bareto/-/commit/dfc13db9c371c828324b82c0c6b6d2c19041259c) - Addchangelog for version 1.2.0
- [c36ca04](https://framagit.org/YannKervran/Bareto/-/commit/c36ca04974d54cceb711f38fd5018332ad391957) - Cleaning
- [d0b6456](https://framagit.org/YannKervran/Bareto/-/commit/d0b645608ac8820442235afcd0c5cfc4a63d2ce3) - YannKervran/Bareto
- [32c1048](https://framagit.org/YannKervran/Bareto/-/commit/32c1048cee759983739b76d0525466cf07e451a7) - Addchangelog for version 1.2.0
- [9a5fbcf](https://framagit.org/YannKervran/Bareto/-/commit/9a5fbcfeef3b2fcf267702b452214b83eb846049) - Untroisième ajout
- [226530e](https://framagit.org/YannKervran/Bareto/-/commit/226530e032fcc555dcdf8d0016c08a3f3572e4f4) - Unsecond ajout
- [2002417](https://framagit.org/YannKervran/Bareto/-/commit/20024172915086ef904bbfedc4d8cd1e33a29ca1) - Nouvelajout
- [c8bd97b](https://framagit.org/YannKervran/Bareto/-/commit/c8bd97b4d547ac12813e29ccd6f1862147932a8d) - Addchangelog for version 1.1.3
- [762b89a](https://framagit.org/YannKervran/Bareto/-/commit/762b89a7d9f793e8e0562a8d34b24224198b827f) - Addchangelog for version 1.1.2
- [a0c6005](https://framagit.org/YannKervran/Bareto/-/commit/a0c6005ceea246011037db8e0e8a07e17fbbef28) - Addchangelog for version 1.0.1
- [cc30a8e](https://framagit.org/YannKervran/Bareto/-/commit/cc30a8e446858ce906e95011c56eb05822ff60d0) - Addchangelog for version 1.0.0
- [980618f](https://framagit.org/YannKervran/Bareto/-/commit/980618f2aa2d737d5d9d86f88cdb53115fb7bc46) - YannKervran/Bareto
- [2ad98a5](https://framagit.org/YannKervran/Bareto/-/commit/2ad98a5dfc212f57ec7609a46ce893c84b87c009) - Addchangelog for version 1.0.0

## [1.1.3] - 2022-09-27
### Feat
- [849c3e7](https://framagit.org/YannKervran/Bareto/-/commit/849c3e7c199a93ef2be947552976c96763209e25) - Adding proper instructions

## [1.1.2] - 2022-09-27
### Docs
- [110c825](https://framagit.org/YannKervran/Bareto/-/commit/110c82504ac8321bd98cfd6eb6ca1da8478e9265) - Use proper web adress for Changelog
- [1d6af5f](https://framagit.org/YannKervran/Bareto/-/commit/1d6af5fd423dfe2bc21227275c3692ed40d1a53f) - Updating CHANELOG with the script
### Feat
- [dce126f](https://framagit.org/YannKervran/Bareto/-/commit/dce126fe697ae2ad4160ce51bbcf0dfe42bb8383) - Added proper adress
### Fix
- [8523e65](https://framagit.org/YannKervran/Bareto/-/commit/8523e65b54e68c538f7386850bcbe0133e2a9ac3) - Using correct path to commit on GitLab

## [1.1.1] - 2022-09-26
### Style
- [faf8c9b](https://framagit.org/YannKervran/Bareto/-/commit/faf8c9b87c8ae97d95d0305ff436e8aab5bec4f1) - Reorganizing commentaries and parameters

## [1.1.0] - 2022-09-26
### Feat
- [30d8355](https://framagit.org/YannKervran/Bareto/-/commit/30d8355963a8727fda02280c9b9f011f4926d054) - Adding dates to tags
### Fix
- [b59ff3c](https://framagit.org/YannKervran/Bareto/-/commit/b59ff3c441d90a4ae18bafd4fbc60a88cf7bb9eb) - Debugging content generation when updating
- [40c6b92](https://framagit.org/YannKervran/Bareto/-/commit/40c6b928e408d1003faafcd93f8a4b2c9cac2086) - Adding header and new item when Unreleased
### Style
- [d2db2ea](https://framagit.org/YannKervran/Bareto/-/commit/d2db2ea7514d3b49042c61606349c19f18242176) - Some debug cleaning

## [1.0.0] - 2022-09-26
### Feat
- [e836f91](https://framagit.org/YannKervran/Bareto/-/commit/e836f9154ce97fd85d10ad0ae0f5922c1a5fbbce) - Adding proper listing from first tag
- [4cdfde8](https://framagit.org/YannKervran/Bareto/-/commit/4cdfde8be9a47e4c4528118de239bacbaf9299bc) - Adding first iteration of history
### Fix
- [6a7c01c](https://framagit.org/YannKervran/Bareto/-/commit/6a7c01cae478f46a3aa496078c958ffa04008efa) - Preventing from creating empty title 3 when no commits are Unreleased
### Refactor
- [822687b](https://framagit.org/YannKervran/Bareto/-/commit/822687b31a85ea3b098884230edd003c49546549) - Better factorisation for new item
- [5d7a1b5](https://framagit.org/YannKervran/Bareto/-/commit/5d7a1b530c9e6b1dd4cbd5c1286abedc7f1e4be7) - Factorize to simplify commits list

## [0.1.0] - 2022-09-24
### Feat
- [9c490d9](https://framagit.org/YannKervran/Bareto/-/commit/9c490d984c68642ba273b85bbd910828ffe6894f) - Adding unreleased commits
### Refactor
- [50a6865](https://framagit.org/YannKervran/Bareto/-/commit/50a6865e951b0a58f0a94d54178434e1386f4be2) - Organizing the content by category for the changelog
- [c631699](https://framagit.org/YannKervran/Bareto/-/commit/c6316990e3f69097ca143fddce5d1b8a8df33ed0) - Organizing display of informations in lines of commits
- [76b9712](https://framagit.org/YannKervran/Bareto/-/commit/76b9712c4c12ab9f0392c6c99bdbfdbc3f3c196b) - Reorganize commit descriptions
### Style
- [670f7b5](https://framagit.org/YannKervran/Bareto/-/commit/670f7b59e8860b492ec58919a1d2c48110967d00) - Refactoring new item function
### Unclassified
- [b6c10ea](https://framagit.org/YannKervran/Bareto/-/commit/b6c10ea58315e7ebf0377f7ea9ad56a59bd97cf2) - Functionning base function for commits by category

## [0.0.3] - 2022-09-24
### Style
- [4676667](https://framagit.org/YannKervran/Bareto/-/commit/467666754753ecd531ce8768d652bac3923000a7) - Cleaning unuseful stuff
- [01d7a3c](https://framagit.org/YannKervran/Bareto/-/commit/01d7a3c49d0f09a8cc9669637fa267a62d6287d1) - Modified echoing
- [ec9a0e6](https://framagit.org/YannKervran/Bareto/-/commit/ec9a0e6dbf9d6b17b2bcd97c0b1d2079566af350) - Modifying the variables
- [784cacd](https://framagit.org/YannKervran/Bareto/-/commit/784cacdd5bb6d07ee4071f3e5cb1229380d4b965) - Better organization of file

## [0.0.2] - 2022-09-24
### Style
- [cb3954b](https://framagit.org/YannKervran/Bareto/-/commit/cb3954b0917bbcca1e4236537085fe99404372f5) - Change initial content

## [0.0.1] - 2022-09-24
### Unclassified
- [3a0e1e8](https://framagit.org/YannKervran/Bareto/-/commit/3a0e1e88744194567e1f31452ef15d4390b446c0) - Newvariables
- [7abcb91](https://framagit.org/YannKervran/Bareto/-/commit/7abcb9106faf6af7cd53a8b3ab4ae24cea542c10) - VersionningOK

## [0.0.0] - 2022-09-24
### Unclassified
- [f904fcf](https://framagit.org/YannKervran/Bareto/-/commit/f904fcf59f43e33a972699bfd693ce72ee5253b4) - Version from git command
- [98967c9](https://framagit.org/YannKervran/Bareto/-/commit/98967c968f67c5a4ec96f8ec0901a3a9d3e311d6) - Initialcommit
