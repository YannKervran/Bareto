#!/bin/bash

# Generate a Markdown changelog of merge requests from commits
# Author: Yann Kervran
# URL: https://framagit.org/YannKervran/Bareto

# HOW TO USE
# Copy this script in Git repository
# Make the script executable: chmod u+x changelog_generator.sh
# Run it: ./changelog_generator.sh
# If you want to have the last entries validated
# as a proper tag and not as an "Unreleased" version,
# you must add a parameter : " --pretag"
# So you then commit the Changelog
# and tag the version with its completed version 

# Get result in CHANGELOG.md (default name, can be changed below) to see your results
# Please check/modify parameters in the section below

# Get parameters
. bareto.cfg

# Get usefull tags from git repository
GIT_VERSIONS=$(git tag -l --sort=-version:refname)
VERSIONS=($GIT_VERSIONS)
LATEST_VERSION=${VERSIONS[0]:-0.0.0}
PREVIOUS_VERSION=${VERSIONS[1]}

# Specify formatting
PRETTY="%H/%h/%s"

# Variables
CATEGORIES_ARRAY=()
PARAM1=$1

# Functions

add_commits()
# Adding commits organized by category
# Needs 2 arguments in order :
# 1 - Beginning tag
# 2 - Ending tag (or HEAD)
# If there is just one argument, it will give
# the commits list from the beginning
{
	OLD_VERSION=$1
	NEW_VERSION=$2
			# Get content from proper array
			if [[ ! $NEW_VERSION = "" ]]; then
				COMMITS=$(git log --pretty=tformat:"$PRETTY;" $OLD_VERSION..$NEW_VERSION)
			elif [[ $OLD_VERSION = "0.0.0" ]]; then
				COMMITS=$(git log --pretty=tformat:"$PRETTY;")
			else
				COMMITS=$(git log --pretty=tformat:"$PRETTY;" $OLD_VERSION)
			fi
			echo -e $COMMITS > test.txt
			IFS=';' read -ra COMMITS_ARRAY <<< $(cat test.txt)
			rm test.txt
			for COMMIT in "${COMMITS_ARRAY[@]}"; do
				COMMIT=$(echo "$COMMIT" | xargs)
				FULL_HASH=$(echo "$COMMIT" | cut -c 1-40)
				HASH=$(echo "$COMMIT" | cut -c 42-48)
				TITLE=$(echo "$COMMIT" | cut -c 50-)
				CATEGORY=${TITLE%:*}
				CATEGORY="${CATEGORY//!}"
				TITLE=${TITLE#*:}
				TITLE="${TITLE/ /}"
				TITLE=${TITLE^}
				# Discard auto commits  for the changelog from the list
				if [[ ${COMMIT_MSG} =~ ${TITLE} ]]; then continue ; fi
				COMMITLINE="- [$HASH]($REPOSITORY_URL/-/commit/$FULL_HASH) - $TITLE"
				if  [[ ! ${ACCEPTED_CATEGORIES[*]} =~ "$CATEGORY" ]]; then
					CATEGORY="Unclassified"
				fi
				# Create categories array
				if [[ ${#CATEGORIES_ARRAY[@]} -lt 1 ]]; then
						CATEGORIES_ARRAY+=("$CATEGORY")
				else
					if  [[ ! ${CATEGORIES_ARRAY[*]} =~ $CATEGORY ]]; then
						CATEGORIES_ARRAY+=("$CATEGORY")
					fi
				fi
				# Organize commits to proper category through files
				for (( GROUP=0; GROUP<${#CATEGORIES_ARRAY[@]};GROUP++ )); do
					if [[ ${CATEGORIES_ARRAY[GROUP]} == $CATEGORY ]]; then
						echo -e $COMMITLINE >> cat_${CATEGORIES_ARRAY[GROUP]}.txt
					fi
				done
			done
			# Write markdown content from the files
			if ls cat_*.txt 1> /dev/null 2>&1 ; then
				CATEGORY_FILES=(cat_*.txt)
				for (( GROUP=0; GROUP<${#CATEGORY_FILES[@]};GROUP++ )); do
					CATEGORYTITLE=${CATEGORY_FILES[GROUP]}
					CATEGORYTITLE=${CATEGORYTITLE#*_}
					CATEGORYTITLE=${CATEGORYTITLE%.*}
					CATEGORYTITLE=${CATEGORYTITLE^}
					MARKDOWN+="### $CATEGORYTITLE\n"
					MARKDOWN+=$(cat ${CATEGORY_FILES[GROUP]})
					MARKDOWN+="\n"
					rm ${CATEGORY_FILES[GROUP]}
				done
			fi
}

new_history()
# Creates history when the changelog is initiated
{
	for (( INDEX=0; INDEX<${#VERSIONS[@]}; INDEX++ )); do
		OLD=${VERSIONS[$INDEX+1]}
		NEW=${VERSIONS[$INDEX]}
		DATE=$(git log -1 --format=%as "$NEW")
		MARKDOWN="## [$NEW] - $DATE\n"
		add_commits $OLD $NEW
		echo -e "$MARKDOWN" >> $CHANGELOGFILE
	done
}

new_changelog_item()
# Adding new commit(s) to changelog
{
	MARKDOWN=""
	# Get the content of Changelog file without header content
	while IFS= read -r LINE; do
		sed -i '1d' $CHANGELOGFILE
	done < <(printf '%s\n' "$HEADER")
	# Take out the possible Unreleased content or the Initial untagged content
	# And keep the rest of the file
	if [[ "$LATEST_VERSION" == "0.0.0" ]]; then TYPEUP=$(./getchangetype.sh); else TYPEUP=$(./getchangetype.sh $LATEST_VERSION..HEAD); fi
	if [ -z "${LATEST_VERSION}" ]; then NEWVER=$(./version_incrementer.sh 0.0.0 $TYPEUP); else NEWVER=$(./version_incrementer.sh $LATEST_VERSION $TYPEUP); fi
	if [[ $(head -n 1 "$CHANGELOGFILE") =~ "## [Unreleased" ]]; then
		FILECONTENT=$(sed -n '1,/\[[[:digit:]]\+\.[[:digit:]]\+\.[[:digit:]]\+\]/{x;d;};1h;1!{x;p;};${x;p;}' $CHANGELOGFILE)
	elif [[ $(head -n 1 "$CHANGELOGFILE") =~ "## [$NEWVER" ]] && [[ ! $(git log $LATEST_VERSION..HEAD) == "" ]]; then
		FILECONTENT=$(sed -n '1,/\[[[:digit:]]\+\.[[:digit:]]\+\.[[:digit:]]\+\]/{x;d;};1h;1!{x;p;};${x;p;}' $CHANGELOGFILE)
	elif [[ $(head -n 1 "$CHANGELOGFILE") =~ "## [$INIT_VERSION" ]] && [[ ! $(git log) == "" ]]; then
	echo "Case 1"
		FILECONTENT=$(sed -n '1,/\[[[:digit:]]\+\.[[:digit:]]\+\.[[:digit:]]\+\]/{x;d;};1h;1!{x;p;};${x;p;}' $CHANGELOGFILE)
	elif [[ $(head -n 1 "$CHANGELOGFILE") =~ "## [$INIT_VERSION" ]] && [[ ! $(git log $LATEST_VERSION..HEAD) == "" ]]; then
	echo "Case 2"
		FILECONTENT=$(sed -n '1,/\[[[:digit:]]\+\.[[:digit:]]\+\.[[:digit:]]\+\]/{x;d;};1h;1!{x;p;};${x;p;}' $CHANGELOGFILE)
	else
		FILECONTENT=$(cat "$CHANGELOGFILE")
	fi
	#~ # Get the proper commits to add :
	#~ # - Unreleased ones or from freshly created tag
	echo "DEBUG: $LATEST_VERSION"
	if [[ $LATEST_VERSION == "0.0.0" ]]; then
		# For untagged initial commits
	echo "Initial untagged repository"
		# Adding the commits since beginning
		TODAY=$(date +"%Y-%m-%d")
		if [[ $PARAM1 == "--pretag" ]]; then
			MARKDOWN+="## [$NEWVER] - $TODAY\n"
		else
			MARKDOWN+="## [$INIT_VERSION] - $TODAY\n"
		fi
		add_commits $LATEST_VERSION
		echo -e "$HEADER" > $CHANGELOGFILE
		echo -e "$MARKDOWN" >> $CHANGELOGFILE
	elif [[ $LATEST_VERSION && ! $(git log $LATEST_VERSION..HEAD) == "" ]]; then
		# Adding the commits since last tag
		if [[ $PARAM1 == "--pretag" ]]; then
			TODAY=$(date +"%Y-%m-%d")
			MARKDOWN+="## [$NEWVER] - $TODAY\n"
		else
			MARKDOWN+="## [Unreleased - $NEWVER]\n"
		fi
		add_commits $LATEST_VERSION "HEAD"
		echo -e "$HEADER" > $CHANGELOGFILE
		echo -e "$MARKDOWN" >> $CHANGELOGFILE
	else
		if grep -Exq "## \[$LATEST_VERSION\] - [0-9]{4}-[0-9]{2}-[0-9]{2}" $CHANGELOGFILE; then
			echo -e "$HEADER" > $CHANGELOGFILE
		elif grep -Exq "## \[$INIT_VERSION\] - [0-9]{4}-[0-9]{2}-[0-9]{2}" $CHANGELOGFILE; then
			echo -e "$HEADER" > $CHANGELOGFILE
		else
			# Adding the commits between last tags
			if [[ $LATEST_VERSION == "0.0.0" ]]; then
				CHAPTER="$INIT_VERSION" && DATE=$(date +"%Y-%m-%d")
			else CHAPTER="$LATEST_VERSION" && DATE=$(git log -1 --format=%as "$LATEST_VERSION")
			fi
			MARKDOWN+="## [$CHAPTER] - $DATE\n"
			echo "Ajouts commits entre $PREVIOUS_VERSION et $LATEST_VERSION"
			add_commits $PREVIOUS_VERSION $LATEST_VERSION
			echo -e "$HEADER" > $CHANGELOGFILE
			echo -e "$MARKDOWN" >> $CHANGELOGFILE
		fi
	fi
	echo -e "$FILECONTENT" >> $CHANGELOGFILE
}

new_changelog()
# For initialisation of changelog
{
	echo -e "$HEADER" > $CHANGELOGFILE
	new_history
	new_changelog_item
}

main()
# Main function
{
	if test -f "$CHANGELOGFILE" ; then
		new_changelog_item
	else
		new_changelog
	fi
}

# Script
main
