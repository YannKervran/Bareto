# BARETO - BAsh REpository TOols

The Bareto aims to provide simple bash script to ease some repository handling.

## Easy Changelog generator

The first tools will generate automatically a Changelog markdown text. It is conceived to be used in a CI, but it can also be used locally to generate the Changelog without any CI job. It will provide correct [Semantic versionning](https://semver.org/spec/v2.0.0.html) for each new tagged release. The generated Changelog markdown list doesn’t include the automatic commits made by the CI to update the file.

You will be able to generate a Changelog with the `write_changelog` job or to tag a new version and write its changelog with the `tag_version` one.

You must write down your commits’ titles with a strict syntax : 

    category: My title

If you want to introduce a breaking change, you must add an exclamation point at the end of the category :

    category!: My title

You can define a `bugfix_category` list of categories in `bareto.cfg` to let them be recognized as such.

If you use a non-listed category, the commit will be displayed in an "Uncategorized" section in the changelog.

To use the system :
- create a `bareto.cfg` file with proper parameters set in your repository, in root folder (variables are explained in the joined file itself) ;
- write similars jobs to `write_changelog` and `tag_version` of the `example.gitlab-ci.yml` in your repository CI ;
- create in `Settings > Access tokens` in your GitLab repository a token with a `Maintainer` access with `read` and `write` accesses ;
- create in `Settings > CI/CD` the following variables for the agent in charge of the CI operations :
  - `GITLAB_TOKEN` for the token you have created, with `Masked` and `Expanded` checked ;
  - `GITLAB_USERNAME` for the name of the agent, with `Masked` and `Expanded` checked ;

### Scripts used for this task

#### changelog-generator.sh

Generate a proper markdown Changelog based upon [Semantic versionning](https://semver.org/spec/v2.0.0.html). The version generated are taking in account the last tag and the content of commits’ titles. If you add the "--pretag" parameter, it will add a calculated new version instead of an "Unreleased" content part.

The parameters you can define in `the bareto.cfg` file are :
- the URL of the Gitlab repository (for the links to the commits)
- the name of the changelog file
- The content of the header text
- the list of categories accepted (if a commit is not in those, it will be displayed in an "Unclassified" section.

The commits’ titles must include at their beginning the same words as the categories defined just before (and in the **getchangetype.sh** script below), with a colon ":" to separate them from proper title. If not, they will not be taken in account.

#### getchangetype.sh

This script will browse the commits titles to return the bigger change encountered : major, minor or bugfix.

The parameters you can define in `the bareto.cfg` file are :
- the string returned as Major, Minor or Bugfix type
- the categories defined as bugfixes

The titles must conform to some rules : they must include the category (as defined in the **changelog-generator** script upper), an exclamation mark "!" if the commit is a breaking change, then a colon ":", then a space then the proper title.

#### getlasttag.sh

Very simple script to get the last tag used in a repository.

#### version incrementer

This script will return a new version based upon [Semantic versionning](Semantic Versioning: https://semver.org/spec/v2.0.0.html), with the former version and the type of upgrade asked for (major, minor or bugfix).

#### gitlab-ci.yml

The tag_version job will generate/update the changelog of the repository and tag this last commit with proper version, based upon [Semantic versionning](Semantic Versioning: https://semver.org/spec/v2.0.0.html), the former version and the most important type of commits in-between (major, minor or bugfix). The write_changelog doesn’t tag but fills the changelog with an 'Unreleased’ state with a possible version. Both are manually triggered in this repository.

