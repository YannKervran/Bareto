#!/bin/bash

# Define version for new tag : major/minor/bugfix
# Author: Yann Kervran
# URL: https://framagit.org/YannKervran/Bareto

# HOW TO USE
# Copy this script in Git repository
# Make the script executable: chmod u+x getchangetype.sh
# Run it: ./getchangetype.sh beginning_tag ending_tag
#
# If you define only one tag,
# it will check the list of commits until this one.
# If you give none, it will check the whole list of commits
#
# It returns the type, based upon variables defined below :
# BUGFIX, MINOR, MAJOR
# It will recognize a MAJOR if there is a "!" in the commit title
# It will recognize a BUGFIX if it is in BUGFIX_CAT array below
#
# Base upon Semantic Versioning: https://semver.org/spec/v2.0.0.html


# Get parameters
. bareto.cfg

# Capture entries
OLD=$1
NEW=$2

# Variables
BUG=$(printf "|%s" ${BUGFIX_CAT[*]})
BUG="${BUG:1}"
TITLES=""

if [ ! -z "${OLD}" ] && [ ! -z "${NEW}" ]; then
	TITLES=$(git log --pretty=tformat:"%s;" $OLD..$NEW)
elif [ ! -z "${OLD}" ]; then
	TITLES=$(git log --pretty=tformat:"%s;" $OLD)
else
	TITLES=$(git log --pretty=tformat:"%s;")
fi

echo -e $TITLES > test.txt
IFS=';' read -ra TITLES_ARRAY <<< $(cat test.txt)
rm test.txt
for TITLE in "${TITLES_ARRAY[@]}"; do
	TITLE=$(echo "$TITLE" | xargs)
	TITLE=${TITLE%:*}
	if [[ ${TITLE: -1} == "!" ]]; then
		VERSION_INCREMENTER_ARG=$MAJOR
		break
	elif [[ ${BUGFIX_CAT[*]} =~ "$TITLE" ]] && [[ VERSION_INCREMENTER_ARG!=$MINOR ]] ; then
		VERSION_INCREMENTER_ARG=$BUGFIX
	else
		VERSION_INCREMENTER_ARG=$MINOR
	fi

	HEADERS+="$TITLE"
done

echo "$VERSION_INCREMENTER_ARG"
